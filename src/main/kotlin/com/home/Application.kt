package com.home

import com.home.dao.DAOFacade
import com.home.dao.DAOFacadeImplementation
import com.home.dao.DatabaseFactory
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.resources.*
import io.ktor.server.netty.*
import com.home.plugins.*
import java.io.File
import java.io.IOException

fun main(args: Array<String>): Unit =
    io.ktor.server.netty.EngineMain.main(args)
@Suppress("unused") // application.conf references the main function. This annotation prevents the IDE from marking it as unused.
fun Application.module() {

    val dao: DAOFacade = DAOFacadeImplementation()

    val config = environment.config.config("home")
    val uploadDirPath: String = config.property("upload.dir").getString()
    val uploadDir = File(uploadDirPath)
    if (!uploadDir.mkdirs() && !uploadDir.exists()) {
        throw IOException("Failed to create directory ${uploadDir.absolutePath}")
    }

    DatabaseFactory.init()
    configureHTTP()
    configureSerialization()
//    configureSecurity(dao)
    configureRouting(dao)

}

