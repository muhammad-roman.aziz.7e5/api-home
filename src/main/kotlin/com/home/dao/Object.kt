package com.home.dao

import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.ReferenceOption

object Object : Table() {
    val objectId = integer("object_id").autoIncrement()
    val roomId = integer("room_id") references Room.roomId
    val isOn = bool("isOn")
    val lastTimeOpened = long("lastTimeOpened")
    val timeOpened = long("timeOpened")

    override val primaryKey = PrimaryKey(objectId)

}