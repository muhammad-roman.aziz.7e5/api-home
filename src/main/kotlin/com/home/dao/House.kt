package com.home.dao

import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.ReferenceOption

object House : Table() {
    val houseId = integer("house_id").autoIncrement()
    val address = varchar("address", length = 255)
    val ownerId = integer("owner_id") references Usuari.userId

    override val primaryKey = PrimaryKey(houseId)

}