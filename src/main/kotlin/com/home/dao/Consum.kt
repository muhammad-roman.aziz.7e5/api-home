package com.home.dao

import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.ReferenceOption

object Consum : Table() {
    val consumId = integer("consum_ID").autoIncrement()
    val objectId = integer("object_id") references Object.objectId
    val opened = long("opened")
    val closed = long("closed")
    val mitjanaConsum = integer("mitjanaConsum")

    override val primaryKey = PrimaryKey(consumId)

}