package com.home.dao


import com.home.dao.DatabaseFactory.dbQuery
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.selectAll
import com.home.model.*
import org.jetbrains.exposed.sql.insert

class DAOFacadeImplementation:DAOFacade {

    private fun resultRowToUser(row: ResultRow): com.home.model.Usuari = Usuari(
        user_id = row[Usuari.userId],
        name = row[Usuari.name],
        password = row[Usuari.password],
        profilePic = row[Usuari.profilePic],
        email = row[Usuari.email],

    )
    override suspend fun allUsers(): List<com.home.model.Usuari> = dbQuery {
        Usuari.selectAll().map(::resultRowToUser)
    }

    override suspend fun user(id: Int): Usuari? {
        TODO("Not yet implemented")
    }

    override suspend fun addUser(email: String, password: String,name: String,profilePic: String): Usuari? = dbQuery {
        val insertStatement = Usuari.insert {
            it[Usuari.name] = name
            it[Usuari.email] = password
            it[Usuari.profilePic] = profilePic
        }

    }

    override suspend fun editUser(id: Int, email: String, password: String): Boolean {
        TODO("Not yet implemented")
    }

    override suspend fun deleteUser(id: Int): Boolean {
        TODO("Not yet implemented")
    }

    override suspend fun authenticateUser(email: String, password: String): Usuari? {
        TODO("Not yet implemented")
    }

    override suspend fun addHouse(address: String, owner_id: Int, house_id: Int): House? {
        TODO("Not yet implemented")
    }

    override suspend fun editHouse(house_id: Int, address: String, owner_id: Int): Boolean {
        TODO("Not yet implemented")
    }

    override suspend fun deleteHouse(house_id: Int): Boolean {
        TODO("Not yet implemented")
    }

    override suspend fun addRoom(house_id: Int, room_id: Int): House? {
        TODO("Not yet implemented")
    }

    override suspend fun editRoom(house_id: Int, room_id: Int): Boolean {
        TODO("Not yet implemented")
    }

    override suspend fun deleteRoom(room_id: Int): Boolean {
        TODO("Not yet implemented")
    }
}