package com.home.dao

interface DAOFacade {

    suspend fun allUsers(): List<com.home.model.Usuari>
    suspend fun user(id: Int): Usuari?
    suspend fun addUser(email: String, password: String,name: String,profilePic: String): Usuari?
    suspend fun editUser(id: Int, email: String, password: String): Boolean
    suspend fun deleteUser(id: Int): Boolean
    suspend fun authenticateUser(email: String, password: String): Usuari?

    suspend fun addHouse(address: String,owner_id: Int,house_id: Int):House?
    suspend fun editHouse(house_id: Int,address: String,owner_id: Int):Boolean
    suspend fun deleteHouse(house_id: Int):Boolean

    suspend fun addRoom(house_id: Int,room_id:Int):House?
    suspend fun editRoom(house_id: Int,room_id:Int):Boolean
    suspend fun deleteRoom(room_id:Int):Boolean

//    suspend fun createObject(object_id:Int,room_id: Int,isOn:Boolean)


}