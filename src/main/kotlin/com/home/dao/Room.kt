package com.home.dao

import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.ReferenceOption

object Room : Table() {
    val roomId = integer("room_id")
    val houseId = integer("house_id") references House.houseId

    override val primaryKey = PrimaryKey(roomId)

}