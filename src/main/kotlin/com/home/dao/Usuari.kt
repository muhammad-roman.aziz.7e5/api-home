package com.home.dao

import org.jetbrains.exposed.sql.Table

object Usuari : Table() {
    val userId = integer("user_id").autoIncrement()
    val name = varchar("name", length = 255)
    val email = varchar("email", length = 255)
    val password = varchar("password", length = 255)
    val profilePic = varchar("profilePic", length = 1000)

    override val primaryKey = PrimaryKey(userId)
}