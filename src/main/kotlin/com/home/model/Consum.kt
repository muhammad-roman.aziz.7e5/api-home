package com.home.model

import kotlinx.serialization.Serializable

@Serializable
data class Consum(
    val consum_ID: Int,
    var room_id: Int,
    var opened: Long,
    var closed: Long,
    var mitjanaConsum: Int
)
