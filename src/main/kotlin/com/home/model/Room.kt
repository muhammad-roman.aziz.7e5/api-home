package com.home.model


import kotlinx.serialization.Serializable

@Serializable
data class Room(
    var room_id: Int,
    var house_id: Int,
    var roomName: String,
    var objects: List<Object>
){

    fun getHouse(): House {
        TODO("")
    }


    fun getAllObjects(){
        TODO()
    }

    fun storeRoom(): Boolean {

        return false
    }


    companion object {

    }

}
