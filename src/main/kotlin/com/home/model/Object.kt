package com.home.model

import kotlinx.serialization.Serializable


@Serializable
class Object (
    var object_id: Int,
    var room_id: Int,
    var objectsName: String,
    var isOn: Boolean,
    var lastTimeOpened: Long,
    var timeOpened: Long) {

}