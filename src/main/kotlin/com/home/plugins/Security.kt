package com.home.plugins

import com.home.dao.DAOFacade
import io.ktor.server.sessions.*
import io.ktor.server.response.*
import io.ktor.server.auth.*
import io.ktor.server.application.*
import io.ktor.server.routing.*

//    fun Application.configureSecurity(dao: DAOFacade) {
//        install(Authentication) {
//            basic("auth-basic") {
//                realm = "Access to the '/' path"
//                validate { credentials ->
//                    val user = dao.getUserByUsername(credentials.name)
//                    if (user != null && user.password == credentials.password) {
//                        UserIdPrincipal(credentials.name)
//                    } else {
//                        null
//                    }
//                }
//            }
//        }
//    }