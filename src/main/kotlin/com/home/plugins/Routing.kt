package com.home.plugins

import com.home.dao.DAOFacade
import com.home.routes.getUsers
import io.ktor.server.routing.*
import io.ktor.server.resources.*

import io.ktor.server.application.*

fun Application.configureRouting(daoFacade: DAOFacade) {
    install(Resources)
    routing {
        getUsers(daoFacade)
    }
}
